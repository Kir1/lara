@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('main.product.edit') }}</div>

                    <div class="panel-body">
                        @include('product.form.create_update', [
                            'route' => [
                                'product.create_update',
                                $product->id
                            ],
                            $product
                        ])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
