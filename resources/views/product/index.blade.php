@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('main.product.products') }}</div>

                    <div class="panel-body">
                        @include('product.form.create_update', ['route'=>'product.create_update'])

                        @if ($productsHtml)
                            {{ trans('main.product.hint') }}

                            <ol class="sortable">
                                {!! $productsHtml !!}
                            </ol>

                            <div class='hide alert alert-info'>{{ trans('main.load') }}</div>
                            <button class="save-hierarchy"
                                    data-url="{{ route('product.hierarchy') }}">{{ trans('main.crud.save') }}</button>
                            <div class='hide alert alert-danger'>{{ trans('main.error.error') }}</div>
                        @else
                            {{ trans('main.not_result') }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
