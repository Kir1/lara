{{ Form::open(['route' => $route ?? 'product.create_update', 'class'=>'form-group']) }}
{{ Form::text('name', $product->name ?? '', ['placeholder'=> trans('main.product.ru_title'),'class'=>'form-control'] ) }}
{{ Form::text('name_eng', $product->name_eng ?? '', ['placeholder'=> trans('main.product.en_title'),'class'=>'form-control']) }}
@if ($productsJson)
    <div class="hide products_json">{{ $productsJson }}</div>
    {{ Form::text('product_name', $product->parent_id ?? '', ['placeholder'=> trans('main.product.chose_parent'),'class'=>'form-control','id'=>'products']) }}
@endif
{{ Form::submit(trans( isset($product) ? 'main.crud.save' : 'main.crud.create' )) }}
{{ Form::close() }}

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (Session::has('success'))
    <div class='alert alert-success'>{{ Session::get('success') }}</div>
@endif