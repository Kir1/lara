@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('main.order.orders') }}</div>

                    <div class="panel-body">
                        {{ Form::open(['route' => 'order.create', 'class'=>'form-group']) }}
                        {{ Form::text('fio', '', ['placeholder'=> trans('main.order.fio'),'class'=>'form-control'] ) }}
                        {{ Form::text('mail', '', ['placeholder'=> trans('main.auth.email_address'),'class'=>'form-control'] ) }}
                        @if ($productsJson)
                            <div class="hide products_json">{{ $productsJson }}</div>
                            {{ Form::text('product_name', '', ['placeholder'=> trans('main.product.chose_parent'),'class'=>'form-control','id'=>'products']) }}
                        @endif
                        {{ Form::submit(trans('main.order.create')) }}
                        {{ Form::close() }}

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if (Session::has('success'))
                            <div class='alert alert-success'>{{ Session::get('success') }}</div>
                        @endif


                        @if (\App\User::check_auth())
                            @if (count($orders)>0)
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>№</th>
                                        <th>{{ trans('main.order.client') }}</th>
                                        <th>{{ trans('main.order.mail') }}</th>
                                        <th>{{ trans('main.order.product') }}</th>
                                        <th>{{ trans('main.order.status_title') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($orders as $num => $order)
                                        <tr class="{{ $order->get_status_class() }}">
                                            <td>{{ $num+1 }}</td>
                                            <td>{{ $order->fio }}</td>
                                            <td>{{ $order->mail }}</td>
                                            <td>{{ $order->productName() }}</td>
                                            <td>
                                                {{ Form::open(['route' => ['order.update',$order->id], 'class'=>'form-group'])  }}
                                                {{
                                                Form::select('status',
                                                \App\Order::$statuses[ \Illuminate\Support\Facades\App::getLocale() ],
                                                $order->status )
                                                }}
                                                {{ Form::submit(trans('main.crud.save'),['class'=>'btn btn-success']) }}
                                                {{ Form::close() }}
                                            </td>
                                            <td>

                                                {{ Form::open(['route' => ['order.destroy',$order->id], 'class'=>'form-group']) }}
                                                {{ Form::submit(trans('main.crud.remove'),['class'=>'btn btn-danger']) }}
                                                {{ Form::close() }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $orders->links() }}
                            @else
                                {{ trans('main.not_result') }}
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
