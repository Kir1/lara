<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ trans('site.title') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <h2>{{ $title }}</h2> <br>
        <b>{{ trans('main.order.client') }}</b>: {{ $order->fio }} <br>
        <b>{{ trans('main.order.mail') }}</b>: {{ $order->email }} <br>
        <b>{{ trans('main.order.status_title') }}</b>: {{ $order->status_name }} <br>
        <b>{{ trans('main.order.product') }}</b>: {{ $order->product_name }} <br>
    </div>
</div>
</body>
</html>
