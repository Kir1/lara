<?php
return [
    'not_result' => 'There are currently no records',
    'load' => 'Load...',
    'record' => [
        'created' => 'Record created',
        'updated' => 'Record updeated',
        'remove' => 'Record deleted'
    ],
    'crud' => [
        'create' => 'Create',
        'edit' => 'Edit',
        'save' => 'Save',
        'remove' => 'Remove'
    ],
    'menu' => [
        'home' => 'Home',
        'login' => 'Login',
        'logout' => 'Logout',
        'language' => 'Language'
    ],
    'auth' => [
        'email_address' => 'E-Mail address',
        'password' => 'Password',
        'remember_me' => 'Remember Me'
    ],
    'site' => [
        'title' => 'Orders of furniture',
        'body' => 'The best furniture for you and your company'
    ],
    'order' => [
        'orders' => 'Orders',
        'fio' => 'Name and surname',
        'client' => 'Client',
        'mail' => 'Mail',
        'product' => 'Product',
        'status_title' => 'Status',
        'create' => 'Create order',
        'not_result' => 'You can leave a request when we have goods in stock',
        'order' => 'Order',
        'status' => [
            'new' => 'created',
            'during' => 'during',
            'closed' => 'closed'
        ]
    ],
    'product' => [
        'ru_title' => 'Russian title',
        'en_title' => 'English title',
        'products' => 'Products',
        'create' => 'Create product',
        'edit' => 'Update product',
        'hint' => 'You can drag items with the left mouse button to change the hierarchy',
        'chose_parent' => 'Select the parent product or leave the field empty',
        'error_parent' => 'Select a parent product that is not the current product',
        'without_parent_product' => 'Without parent product',
        'nothing' => 'Nothing'
    ],
    'error' => [
        'error' => 'Error',
        '404' => 'This page not found',
        '403' => 'You do not have access to this page',
        'desired_language' => 'Enter the string in the desired language'
    ]
];
