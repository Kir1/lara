<?php

use Illuminate\Database\Seeder;
use App\Order;
use App\Product;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $user = new User();
        $user->name = 'User';
        $user->email = 'user@mail.ru';
        $user->password = bcrypt('1234567');
        $user->save();

        $user = new User();
        $user->name = 'Admin';
        $user->email = 'admin@mail.ru';
        $user->password = bcrypt('123456');
        $user->role = User::$roles['admin'];
        $user->save();

        $product1 = new Product();
        $product1->name = 'Офисная мебель';
        $product1->save();

        $product2 = new Product();
        $product2->parent_id = $product1->id;
        $product2->name = 'Письменный стол';
        $product2->save();

        $product3 = new Product();
        $product3->name = 'Полка';
        $product3->save();

        $order = new Order();
        $order->fio = 'Иванов Иван Иванович';
        $order->email = 'client@mail.ru';
        $order->product = $product1->id;
        $order->save();

        $order = new Order();
        $order->fio = 'Евгенов Евгений Евгеньевич';
        $order->email = 'client2@mail.ru';
        $order->product = $product2->id;
        $order->save();
    }
}
