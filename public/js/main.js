(function ($) {
    $(window).load(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('ol.sortable').nestedSortable({
            forcePlaceholderSize: true,
            handle: 'div',
            helper: 'clone',
            items: 'li',
            opacity: .6,
            placeholder: 'placeholder',
            revert: 250,
            tabSize: 25,
            tolerance: 'pointer',
            toleranceElement: '> div',
            maxLevels: 0,
            isTree: true,
            expandOnHover: 700,
            startCollapsed: false
        });

        if ($('#products').length > 0) {
            $('#products').comboTree({
                source: jQuery.parseJSON($('.products_json').html()),
                isMultiple: false
            });
        }

        var saveHierarchy = $('.save-hierarchy');
        if ($(saveHierarchy).length > 0) {
            var loadMessage = $(saveHierarchy).prev();
            var errorMessage = $(saveHierarchy).next();
            $(loadMessage).removeClass('hide');
            $(loadMessage).hide();
            $(errorMessage).removeClass('hide');
            $(errorMessage).hide();

            $('.save-hierarchy').click(function () {
                var products =  JSON.stringify( $('ol.sortable').nestedSortable('toHierarchy', {startDepthCount: 0}) )

                $(saveHierarchy).hide();
                $(loadMessage).show();

                $.post($(this).attr('data-url'), {'products':products})
                    .success(function () {
                       location.reload();
                    })
                    .error(function () {
                        $(errorMessage).show();
                        $(loadMessage).hide();
                        $(saveHierarchy).show();
                    })
            });

            $('.comboTreeArrowBtn').click(function (ev) {
                ev.preventDefault();
            });

        }
    });
})(jQuery);