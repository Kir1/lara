<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('change-language/{locale}', function ($locale) {
    if ($locale != session('lang')) session(['lang' => $locale]);
    return back();
})->name('change_language');

Route::middleware(['lang'])->group(function () {
    Auth::routes();

    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('orders', 'OrderController@index')->name('orders');
    Route::post('order/create', 'OrderController@create')->name('order.create');

    Route::prefix('admin')->group(function () {
        Route::prefix('product')->middleware(['check_admin'])->group(function () {
            Route::get('index', 'ProductController@index')->name('products');
            Route::post('hierarchy', 'ProductController@save_hierarchy')->name('product.hierarchy');
            Route::post('create-update/{idUpdateRecord?}', 'ProductController@create_update')->name('product.create_update');
            Route::get('{id}/edit', 'ProductController@edit')->name('product.edit');
            Route::get('{id}/destroy', 'ProductController@destroy')->name('product.destroy');
        });

        Route::prefix('order')->middleware(['check_auth'])->group(function () {
            Route::post('{id}/update', 'OrderController@update_status')->name('order.update');
            Route::post('{id}/destroy', 'OrderController@destroy')->name('order.destroy');
        });
    });
});

