<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::getProducts();
        return view('product.index', ['productsHtml' => $products['html'], 'productsJson' => $products['json']]);
    }

    /**
     * Сохранение порядка иерархии продуктов
     *
     * @return \Illuminate\Http\Response
     */
    public function save_hierarchy(Request $request)
    {
        $products = json_decode($request->input('products'));
        Product::updateHierarchy($products);
        return back();
    }

    /**
     * Создание записи или редактирование записи(если указан $idUpdateRecord)
     *
     * @param integer $idUpdateRecord id редактируемой записи
     * @return \Illuminate\Http\Response
     */
    public function create_update(Request $request, $idUpdateRecord = 0)
    {
        $errorMessages = [];

        //js плагин возвращает имя, поэтому приходится получать отдельно айди родительского продукта
        $parentId = Product::getProductByName($request);

        //действия при редактировании записи
        if ($idUpdateRecord > 0) {
            $product = Product::find($idUpdateRecord);
            $flash = trans('main.record.updated');
            $rules = [
                'name' => "regex:/^[а-яА-Я0-9 .\-]+$/i|required|max:255|min:3|unique:products,name,$idUpdateRecord",
                'name_eng' => "regex:/^[a-zA-Z0-9 .\-]+$/i",
                'product_name' => "not_in:".$product->name
            ];
            $errorMessages = [
                'product_name.not_in' => trans('main.product.error_parent'),
                'name.regex' => trans('main.error.desired_language')
            ];
        }
        //действия при создании записи
        else {
            $product = new Product;
            $flash = trans('main.record.created');
            $rules = [
                'name' => "regex:/^[а-яА-Я0-9 .\-]+$/i|required|max:255|min:3|unique:products",
                'name_eng' => "regex:/^[a-zA-Z0-9 .\-]+$/i"
            ];
            $errorMessages = [
                'product_name.not_in' => trans('main.product.error_parent'),
                'name.regex' => trans('main.error.desired_language')
            ];
        }

        $product->parent_id = $parentId;
        $product->name = $request->name;

        $this->validate($request, $rules, $errorMessages);

        $product->save();
        $request->session()->flash('success', $flash);

        return back();
    }

    /**
     * Страница редактирования записи
     *
     * @param  \App\Product $product
     * @param integer $id id редактируемой записи
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = Product::getProducts(false);
        $product = Product::find($id);
        //js плагин возвращает имя, поэтому приходится получать отдельно айди родительского продукта
        $parent = Product::where('id', '=', $product->parent_id)->select('name')->first();
        if ($parent) $product->parent_id = $parent->name;
        else $product->parent_id = '';
        return view('product.edit', ['product' => $product, 'productsJson' => $products['json']]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer $id айди записи
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Product::destroy($id);
        Product::where('parent_id', $id)->update(['parent_id' => 0]);
        $request->session()->flash('success',  trans('main.record.remove'));
        return back();
    }
}
