<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::getProducts(false);
        $orders = [];
        if (User::check_auth()) $orders = Order::orderBy('created_at','desc')->paginate(10);
        return view('order.index', ['productsJson' => $products['json'], 'orders' => $orders]);
    }

    /**
     * Создание записи записи
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //js плагин возвращает имя, поэтому приходится получать отдельно айди родительского продукта
        $productId = Product::getProductByName($request);

        $rules = [
            'fio' => "required",
            'mail' => "required|email"
        ];
        $this->validate($request, $rules);

        $order = new Order;
        $order->fio = $request->fio;
        $order->email = $request->mail;
        $order->product = $productId;
        $order->save();

        $request->session()->flash('success', trans('main.record.created'));

        /*
        $order->product_name = $request->product_name;
        Order::orderStatusMail($order);
        */

        return redirect()->route('orders');
    }

    /**
     * Изменение записи
     *
     * @param integer $id редактируемой записи
     * @return \Illuminate\Http\Response
     */
    public function update_status(Request $request, $id = 0)
    {
        $order = Order::find($id);
        $order->status = $request->status;
        $order->save();
        $request->session()->flash('success', trans('main.record.updated'));

        /*
        $order->product_name = Product::getProductName($order->product);
        Order::orderStatusMail($order);
        */

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer $id айди записи
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Order::destroy($id);
        $request->session()->flash('success', trans('main.record.remove'));
        return redirect()->route('orders');
    }
}
