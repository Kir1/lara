<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class IniLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $prevLang = App::getLocale();
        $nextLang = session('lang');

        if ($nextLang == null) {
            $nextLang = $prevLang;
            session(['lang' =>  $nextLang]);
        }
        if ($prevLang != $nextLang) App::setLocale($nextLang);

        return $next($request);
    }
}
