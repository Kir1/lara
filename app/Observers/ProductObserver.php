<?php

namespace App\Observers;

use App\Product;

class ProductObserver
{
    /**
     * Handle to the Product "creating" event.
     *
     * @param  \App\Product $user
     * @return void
     */
    public function creating(Product $product)
    {
        $product->translite();
    }

    /**
     * Handle the Product "updated" event.
     *
     * @param  \App\Product $user
     * @return void
     */
    public function updated(Product $product)
    {
        $product->translite();
    }
}