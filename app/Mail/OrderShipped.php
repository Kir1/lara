<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Заголовок письма
     *
     * @var string
     */
    public $title;

    /**
     * Заказ
     *
     * @var Order
     */
    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($title, $order)
    {
        $this->title = $title;
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('meliastras@gmail.com', trans('main.site.title'))
        ->subject($this->title)
        ->view('mail.order', ['order'=> $this->order]);
    }
}
