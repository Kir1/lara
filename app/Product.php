<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use ElForastero\Transliterate\Transliteration;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class Product extends Model
{
    /**
     * Определяет необходимость отметок времени для модели.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id', 'name', 'name_eng'
    ];

    /**
     * Построить массив для рендера иерархической структуры
     *
     * @param array $elements Массив
     * @param integer $parentId Айди родительской записи
     * @return array
     */
    public static function buildTree($elements, $parentId = 0)
    {
        $branch = array();
        foreach ($elements as $element) {
            if (App::isLocale('ru') || (App::isLocale('en') && $element['name_eng'] == '')) $title = $element['name'];
            else $title = $element['name_eng'];
            $element['title'] = $title;
            if ($element['parent_id'] == $parentId) {
                $children = Product::buildTree($elements, $element['id']);
                if ($children) {
                    $element['subs'] = $children;
                }
                $branch[] = $element;
            }
        }

        usort($branch, function ($a, $b) {
            return ($a['sort'] - $b['sort']);
        });

        return $branch;
    }

    /**
     * Построить html список по массиву с иерархической структурой
     *
     * @param array $array Массив
     * @return array
     */
    public static function renderTree($array)
    {
        $str = '';
        if (is_array($array) && count($array)) foreach ($array as $object) {
            if ($object['id'] == 0) continue;
            if (isset($object['subs'])) $olChildren = '<ol>' . Product::renderTree($object['subs']) . '</ol>';
            else $olChildren = '';
            $str .=
                '<li id = "menuItem_' . $object['id'] . '" >' .
                '<div class="menuDiv">' .
                $object['title'] .
                '  (' .
                '<a class="menuDiv" href= "' . route('product.edit', ['id' => $object['id']]) . '">' . trans('main.crud.edit') . '</a>' .
                ' | ' .
                '<a class="menuDiv" href= "' . route('product.destroy', ['id' => $object['id']]) . '">' . trans('main.crud.remove') . '</a>' .
                ')' .
                '</div>' .
                "$olChildren" .
                "</li>";
        }
        return $str;
    }

    /**
     * Получить масив продуктов из двух массивов - html и json
     *
     * @param boolean $getHtml сформировать html массив
     * @param integer $getJson сформировать json массив
     * @return array
     */
    public static function getProducts($getHtml = true, $getJson = true)
    {
        $array = [];
        $tree = Product::buildTree(Product::all());

        //добавляем для js плагина вариант "без родительского продукта"
        array_unshift($tree, [
            'id' => 0,
            'parent_id' => 0,
            'title' => trans('main.product.without_parent_product')
        ]);

        $array['html'] = [];
        $array['json'] = '';
        if (count($tree) > 0) {
            if ($getHtml) $array['html'] = Product::renderTree($tree);
            if ($getJson) $array['json'] = json_encode($tree);
        }

        return $array;
    }

    /**
     * Получить id продукта по имени из данных формы и инициализировать её в них
     *
     * @param Request $request данные с формы с атрибуом parent_name
     * @return integer
     */
    public static function getProductByName($request)
    {
        $parentId = 0;

        if (isset($request->product_name) && $request->product_name != trans('main.product.without_parent_product')) {

            $parentId = Product::where('name', '=', $request->product_name)->orWhere('name_eng', '=', $request->product_name)->select('id')->first();

            if ($parentId) {
                $parentId = $parentId->id;
                $request->parent_name = $parentId;
            }
        }

        return $parentId;
    }

    /**
     * Получить название продукта по его id
     *
     * @param integer $id индефикатор продукта
     * @return string
     */
    public static function getProductName($id)
    {
        if ($id == 0) return trans('main.product.nothing');

        $product = Product::find($id);
        if ( App::getLocale() == 'ru') $name = $product->name;
        else $name = $product->name_eng;

        return $name;
    }

    /**
     * Перезаписать иерархию продуктов
     *
     * @param array $products Продукты(первичные или дочерние)
     * @param integer $parentId Айди родительского продукта
     * @return void
     */
    public static function updateHierarchy($products, $parentId = 0)
    {
        foreach ($products as $num => $product) {
            if (isset($product->children)) Product::updateHierarchy($product->children, $product->id);

            $product = Product::find($product->id);

            if ($product) {
                $product->parent_id = $parentId;
                $product->sort = $num;
                $product->save();
            }
        }
    }

    /**
     * Создать английскую версию названия, если его нет (когда выбран русский язык)
     *
     * @return void
     */
    public function translite()
    {
        if ($this->name_eng == '') {
            if (App::isLocale('ru')) $this->name_eng = Transliteration::make($this->name);
            else $this->name_eng = $this->name;
        }
    }
}
