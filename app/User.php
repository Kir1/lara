<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * Роли
     *
     * @var array
     */
    public static $roles = [
        'admin' => 1,
        'user' => 2
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Пользователь админ
     *
     * @return boolean
     */
    public static function check_admin()
    {
        return User::check_auth() && Auth::user()->role == 1;
    }

    /**
     * Пользователь авторизован
     *
     * @return boolean
     */
    public static function check_auth()
    {
        return isset(Auth::user()->role);
    }
}
