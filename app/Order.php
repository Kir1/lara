<?php

namespace App;

use App\Mail\OrderShipped;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class Order extends Model
{
    /**
     * Получить имя продукта
     */
    public function productName()
    {
        if ($this->product == 0) return trans('main.product.nothing');

        if ( App::getLocale() == 'ru') $lang = 'name';
        else $lang = 'name_eng';
        $object = DB::table('products')->where('id', $this->product)->select($lang)->first();
        return $object->{$lang};
    }


    /**
     * Статусы на двух языках
     *
     * @var array
     */
    public static $statuses = [
        'ru'=>[
            1 => 'Новое',
            2 => 'В процессе',
            3 => 'Закрыто'
        ],
        'en'=>[
            1 => 'New',
            2 => 'Process',
            3 => 'Closed'
        ]
    ];

    /**
     * Цвета статусов
     *
     * @var array
     */
    public static $statuses_colors = [
        1 => 'alert alert-warning',
        2 => 'alert alert-info',
        3 => 'alert alert-success'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fio','email','product','status'
    ];

    /**
     * Получить класс с цветом для строки меню по статусу
     *
     * @return string
     */
    public function get_status_class()
    {
        return Order::$statuses_colors[$this->status];
    }

    /**
     * Получить строку с обработкой - сделать её первую букву заглавной
     *
     * @param string $str Изначальная строка
     * @param string $encoding Кодировка
     * @return string
     */
    public static function upFirstLetter($str, $encoding = 'UTF-8')
    {
        return mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding).mb_substr($str, 1, null, $encoding);
    }

    /**
     * Рассылка сообщения на почту админам и на другую почту
     *
     * @param Order $order заказ
     * @return void
     */
    public static function orderStatusMail($order)
    {
        //определяем слово для статуса
        $status = 1;
        if ($order->status == null) $order->status = $status;
        if ($order->status == 1) $status = 'main.order.status.new';
        else if ($order->status == 2) $status = 'main.order.status.during';
        else if ($order->status == 3) $status = 'main.order.status.closed';
        $order->status_name = trans($status);
        //формируем заголовок письма
        $title = trans('main.order.order') . " №" . $order->id . ' ' . $order->status_name;
        //переводим регистр первой буквы статуса
        $order->status_name = Order::upFirstLetter($order->status_name);
        //отправляем почту
        $users = User::where('role', 1)->get();
        $users []= $order;
        //foreach($users  as $user) Mail::to($user->email)->send(new OrderShipped($title, $order));
    }
}
